package mailspnpdriver

import (
	"embed"

	"github.com/starter-go/application"
)

const (
	theModuleName     = "gitee.com/bitwormhole/mails-pnp-driver"
	theModuleVersion  = "v0.0.2"
	theModuleRevision = 2
)

////////////////////////////////////////////////////////////////////////////////

const (
	theAgentModuleResPath      = "src/agent/resources"
	theCommonModuleResPath     = "src/common/resources"
	theDispatcherModuleResPath = "src/dispatcher/resources"
	theDriverModuleResPath     = "src/driver/resources"
	theMainModuleResPath       = "src/main/resources"
	theServerModuleResPath     = "src/server/resources"
	theTestModuleResPath       = "src/test/resources"
)

//go:embed "src/agent/resources"
var theAgentModuleResFS embed.FS

//go:embed "src/common/resources"
var theCommonModuleResFS embed.FS

//go:embed "src/dispatcher/resources"
var theDispatcherModuleResFS embed.FS

//go:embed "src/driver/resources"
var theDriverModuleResFS embed.FS

//go:embed "src/main/resources"
var theMainModuleResFS embed.FS

//go:embed "src/server/resources"
var theServerModuleResFS embed.FS

//go:embed "src/test/resources"
var theTestModuleResFS embed.FS

////////////////////////////////////////////////////////////////////////////////

// NewAgentModule ... 代理模块
func NewAgentModule() *application.ModuleBuilder {
	mb := new(application.ModuleBuilder)
	mb.Name(theModuleName + "#agent")
	mb.Version(theModuleVersion)
	mb.Revision(theModuleRevision)
	mb.EmbedResources(theAgentModuleResFS, theAgentModuleResPath)
	return mb
}

// NewCommonModule ...
func NewCommonModule() *application.ModuleBuilder {
	mb := new(application.ModuleBuilder)
	mb.Name(theModuleName + "#common")
	mb.Version(theModuleVersion)
	mb.Revision(theModuleRevision)
	mb.EmbedResources(theCommonModuleResFS, theCommonModuleResPath)
	return mb
}

// NewDispatcherModule ... 最终的发送模块
func NewDispatcherModule() *application.ModuleBuilder {
	mb := new(application.ModuleBuilder)
	mb.Name(theModuleName + "#dispatcher")
	mb.Version(theModuleVersion)
	mb.Revision(theModuleRevision)
	mb.EmbedResources(theDispatcherModuleResFS, theDispatcherModuleResPath)
	return mb
}

// NewServerModule ... HTTP 服务模块
func NewServerModule() *application.ModuleBuilder {
	mb := new(application.ModuleBuilder)
	mb.Name(theModuleName + "#server")
	mb.Version(theModuleVersion)
	mb.Revision(theModuleRevision)
	mb.EmbedResources(theServerModuleResFS, theServerModuleResPath)
	return mb
}

// NewDriverModule ... 驱动模块
func NewDriverModule() *application.ModuleBuilder {
	mb := new(application.ModuleBuilder)
	mb.Name(theModuleName + "#driver")
	mb.Version(theModuleVersion)
	mb.Revision(theModuleRevision)
	mb.EmbedResources(theDriverModuleResFS, theDriverModuleResPath)
	return mb
}

// NewTestModule ...
func NewTestModule() *application.ModuleBuilder {
	mb := new(application.ModuleBuilder)
	mb.Name(theModuleName + "#test")
	mb.Version(theModuleVersion)
	mb.Revision(theModuleRevision)
	mb.EmbedResources(theTestModuleResFS, theTestModuleResPath)
	return mb
}
