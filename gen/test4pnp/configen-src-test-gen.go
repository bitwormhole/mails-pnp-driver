package test4pnp
import (
    p6edb77797 "gitee.com/bitwormhole/mails-pnp-driver/src/test/golang/unit"
    p0ef6f2938 "github.com/starter-go/application"
    pd671d76a1 "github.com/starter-go/mails"
     "github.com/starter-go/application"
)

// type p6edb77797.TestDriverAgentServerDispatcher in package:gitee.com/bitwormhole/mails-pnp-driver/src/test/golang/unit
//
// id:com-6edb77797813a6ed-unit-TestDriverAgentServerDispatcher
// class:
// alias:
// scope:singleton
//
type p6edb777978_unit_TestDriverAgentServerDispatcher struct {
}

func (inst* p6edb777978_unit_TestDriverAgentServerDispatcher) register(cr application.ComponentRegistry) error {
	r := cr.NewRegistration()
	r.ID = "com-6edb77797813a6ed-unit-TestDriverAgentServerDispatcher"
	r.Classes = ""
	r.Aliases = ""
	r.Scope = "singleton"
	r.NewFunc = inst.new
	r.InjectFunc = inst.inject
	return r.Commit()
}

func (inst* p6edb777978_unit_TestDriverAgentServerDispatcher) new() any {
    return &p6edb77797.TestDriverAgentServerDispatcher{}
}

func (inst* p6edb777978_unit_TestDriverAgentServerDispatcher) inject(injext application.InjectionExt, instance any) error {
	ie := injext
	com := instance.(*p6edb77797.TestDriverAgentServerDispatcher)
	nop(ie, com)

	
    com.AppContext = inst.getAppContext(ie)
    com.Sender = inst.getSender(ie)
    com.ToAddr = inst.getToAddr(ie)


    return nil
}


func (inst*p6edb777978_unit_TestDriverAgentServerDispatcher) getAppContext(ie application.InjectionExt)p0ef6f2938.Context{
    return ie.GetContext()
}


func (inst*p6edb777978_unit_TestDriverAgentServerDispatcher) getSender(ie application.InjectionExt)pd671d76a1.Service{
    return ie.GetComponent("#alias-d671d76a169061f84f6814f84b98af24-Service").(pd671d76a1.Service)
}


func (inst*p6edb777978_unit_TestDriverAgentServerDispatcher) getToAddr(ie application.InjectionExt)string{
    return ie.GetString("${mails.test.to-address}")
}


