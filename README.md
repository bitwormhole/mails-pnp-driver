# mails-pnp-driver

The Pull-and-Push driver for golang module( github.com/starter-go/module-email ) 


## 架构

    {driver} ==== {agent} == push to ==> {server} <== pull == {dispatcher}

### driver

    是消息的源头，它把消息推送到 server 并让其处理


### agent

    agent 充当 driver 与 server  之间的中介；
    如果 driver & server 被集成在同一个进程，则不需要使用 agent;
    否则，应该把 agent & driver 集成在同一进程，
    让 agent 把 driver 提交的请求发送给 server;

### server

    以 http 的形式提供服务,  处理消息的推送和拉取

### dispatcher

    从 server 拉取消息，并通过 tomail|sms 发送到最终用户
