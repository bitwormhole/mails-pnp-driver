package driver4pnp
import (
    pee0586e7d "gitee.com/bitwormhole/mails-pnp-driver/lib/pnp"
    p505edb146 "gitee.com/bitwormhole/mails-pnp-driver/src/driver/golang/drivers"
     "github.com/starter-go/application"
)

// type p505edb146.Registry in package:gitee.com/bitwormhole/mails-pnp-driver/src/driver/golang/drivers
//
// id:com-505edb146accc3be-drivers-Registry
// class:class-d671d76a169061f84f6814f84b98af24-DriverRegistry
// alias:
// scope:singleton
//
type p505edb146a_drivers_Registry struct {
}

func (inst* p505edb146a_drivers_Registry) register(cr application.ComponentRegistry) error {
	r := cr.NewRegistration()
	r.ID = "com-505edb146accc3be-drivers-Registry"
	r.Classes = "class-d671d76a169061f84f6814f84b98af24-DriverRegistry"
	r.Aliases = ""
	r.Scope = "singleton"
	r.NewFunc = inst.new
	r.InjectFunc = inst.inject
	return r.Commit()
}

func (inst* p505edb146a_drivers_Registry) new() any {
    return &p505edb146.Registry{}
}

func (inst* p505edb146a_drivers_Registry) inject(injext application.InjectionExt, instance any) error {
	ie := injext
	com := instance.(*p505edb146.Registry)
	nop(ie, com)

	
    com.PNPService = inst.getPNPService(ie)


    return nil
}


func (inst*p505edb146a_drivers_Registry) getPNPService(ie application.InjectionExt)pee0586e7d.Service{
    return ie.GetComponent("#alias-ee0586e7ddfedca367801cc9c58e5879-Service").(pee0586e7d.Service)
}


