package main4pnp
import (
    pa13443291 "gitee.com/bitwormhole/mails-pnp-driver/src/main/golang/example"
     "github.com/starter-go/application"
)

// type pa13443291.Unit in package:gitee.com/bitwormhole/mails-pnp-driver/src/main/golang/example
//
// id:com-a134432916656dd9-example-Unit
// class:
// alias:
// scope:singleton
//
type pa134432916_example_Unit struct {
}

func (inst* pa134432916_example_Unit) register(cr application.ComponentRegistry) error {
	r := cr.NewRegistration()
	r.ID = "com-a134432916656dd9-example-Unit"
	r.Classes = ""
	r.Aliases = ""
	r.Scope = "singleton"
	r.NewFunc = inst.new
	r.InjectFunc = inst.inject
	return r.Commit()
}

func (inst* pa134432916_example_Unit) new() any {
    return &pa13443291.Unit{}
}

func (inst* pa134432916_example_Unit) inject(injext application.InjectionExt, instance any) error {
	ie := injext
	com := instance.(*pa13443291.Unit)
	nop(ie, com)

	


    return nil
}


