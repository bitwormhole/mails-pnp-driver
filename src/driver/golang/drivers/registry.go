package drivers

import (
	"gitee.com/bitwormhole/mails-pnp-driver/lib/pnp"
	"github.com/starter-go/mails"
)

const (
	driverNameSMS   = "sms-pnp-driver"
	driverNameEmail = "email-pnp-driver"
)

// Registry  ...
type Registry struct {

	//starter:component

	_as func(mails.DriverRegistry) //starter:as(".")

	PNPService pnp.Service //starter:inject("#")

}

func (inst *Registry) _impl() mails.DriverRegistry {
	return inst
}

// ListRegistrations ...
func (inst *Registry) ListRegistrations() []*mails.DriverRegistration {

	sms := &myDriverForSMS{parent: inst}

	email := &myDriverForEmail{parent: inst}

	r1 := &mails.DriverRegistration{
		Name:    driverNameEmail,
		Enabled: true,
		Driver:  email,
	}

	r2 := &mails.DriverRegistration{
		Name:    driverNameSMS,
		Enabled: true,
		Driver:  sms,
	}

	return []*mails.DriverRegistration{r1, r2}
}
