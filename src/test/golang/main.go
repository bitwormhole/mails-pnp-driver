package main

import (
	"os"

	"github.com/starter-go/starter"

	"gitee.com/bitwormhole/mails-pnp-driver/modules/pnp"
	"gitee.com/bitwormhole/mails-pnp-driver/src/test/golang/unit"
)

func main() {

	mg := &unit.ModuleGroup{
		Agent:      pnp.AgentModule(),
		Dispatcher: pnp.DispatcherModule(),
		Driver:     pnp.DriverModule(),
		Server:     pnp.ServerModule(),
	}

	m := pnp.ModuleForTest()
	i := starter.Init(os.Args)
	i.MainModule(m)
	i.GetAttributes().SetAttribute("pnp.modules", mg)
	i.WithPanic(true).Run()
}
