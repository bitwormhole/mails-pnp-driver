package drivers

import (
	"context"

	"gitee.com/bitwormhole/mails-pnp-driver/lib/common/dto"
	"github.com/starter-go/mails"
)

////////////////////////////////////////////////////////////////////////////////
// driver

type myDriverForSMS struct {
	parent *Registry
}

func (inst *myDriverForSMS) _impl() mails.Driver {
	return inst
}

func (inst *myDriverForSMS) Accept(cfg *mails.Configuration) bool {
	return cfg.Driver == driverNameSMS
}

func (inst *myDriverForSMS) CreateDispatcher(cfg *mails.Configuration) (mails.Dispatcher, error) {
	disp := &myDispatcherForSMS{
		parent: inst.parent,
		config: cfg,
	}
	return disp, nil
}

////////////////////////////////////////////////////////////////////////////////
// Dispatcher

type myDispatcherForSMS struct {
	parent *Registry
	config *mails.Configuration
}

func (inst *myDispatcherForSMS) _impl() mails.Dispatcher {
	return inst
}

func (inst *myDispatcherForSMS) Accept(c context.Context, msg *mails.Message) bool {
	a1 := msg.FromAddress
	a2 := inst.config.SenderAddress
	return a1 == a2
}

func (inst *myDispatcherForSMS) Send(c context.Context, msg *mails.Message) error {
	obj, err := convertMessageToVO(msg, dto.TypeSMS)
	if err != nil {
		return err
	}
	ser := inst.parent.PNPService
	return ser.Push(c, obj)
}

////////////////////////////////////////////////////////////////////////////////
