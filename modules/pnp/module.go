package pnp

import (
	mailspnpdriver "gitee.com/bitwormhole/mails-pnp-driver"
	"gitee.com/bitwormhole/mails-pnp-driver/gen/agent4pnp"
	"gitee.com/bitwormhole/mails-pnp-driver/gen/dispatcher4pnp"
	"gitee.com/bitwormhole/mails-pnp-driver/gen/driver4pnp"
	"gitee.com/bitwormhole/mails-pnp-driver/gen/server4pnp"
	"gitee.com/bitwormhole/mails-pnp-driver/gen/test4pnp"
	"github.com/starter-go/application"
	"github.com/starter-go/httpagent/modules/httpagent"
	"github.com/starter-go/libgin/modules/libgin"
	"github.com/starter-go/mails/modules/mails"
)

// AgentModule  ...
func AgentModule() application.Module {
	mb := mailspnpdriver.NewAgentModule()
	mb.Components(agent4pnp.ExportComponents)
	mb.Depend(CommonModule())
	mb.Depend(httpagent.Module())
	return mb.Create()
}

// CommonModule  ...
func CommonModule() application.Module {
	mb := mailspnpdriver.NewCommonModule()
	// mb.Components(main4pnp.ExportComponents)
	return mb.Create()
}

// DriverModule  ...
func DriverModule() application.Module {
	mb := mailspnpdriver.NewDriverModule()
	mb.Components(driver4pnp.ExportComponents)
	mb.Depend(CommonModule())
	mb.Depend(mails.LibModule())
	return mb.Create()
}

// ServerModule  ...
func ServerModule() application.Module {
	mb := mailspnpdriver.NewServerModule()
	mb.Components(server4pnp.ExportComponents)
	mb.Depend(CommonModule())
	mb.Depend(libgin.Module())
	return mb.Create()
}

// DispatcherModule  ...
func DispatcherModule() application.Module {
	mb := mailspnpdriver.NewDispatcherModule()
	mb.Components(dispatcher4pnp.ExportComponents)
	mb.Depend(CommonModule())
	mb.Depend(mails.LibModule())
	mb.Depend(httpagent.Module())
	return mb.Create()
}

// ModuleForTest ...
func ModuleForTest() application.Module {
	mb := mailspnpdriver.NewTestModule()
	mb.Components(test4pnp.ExportComponents)

	mb.Depend(AgentModule())
	mb.Depend(DriverModule())

	return mb.Create()
}
