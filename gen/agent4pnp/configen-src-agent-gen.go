package agent4pnp
import (
    p105ddcbe4 "gitee.com/bitwormhole/mails-pnp-driver/src/agent/golang/agents"
    pdea5a0f47 "github.com/starter-go/httpagent"
     "github.com/starter-go/application"
)

// type p105ddcbe4.PNPServiceImpl in package:gitee.com/bitwormhole/mails-pnp-driver/src/agent/golang/agents
//
// id:com-105ddcbe44c4862d-agents-PNPServiceImpl
// class:
// alias:alias-ee0586e7ddfedca367801cc9c58e5879-Service
// scope:singleton
//
type p105ddcbe44_agents_PNPServiceImpl struct {
}

func (inst* p105ddcbe44_agents_PNPServiceImpl) register(cr application.ComponentRegistry) error {
	r := cr.NewRegistration()
	r.ID = "com-105ddcbe44c4862d-agents-PNPServiceImpl"
	r.Classes = ""
	r.Aliases = "alias-ee0586e7ddfedca367801cc9c58e5879-Service"
	r.Scope = "singleton"
	r.NewFunc = inst.new
	r.InjectFunc = inst.inject
	return r.Commit()
}

func (inst* p105ddcbe44_agents_PNPServiceImpl) new() any {
    return &p105ddcbe4.PNPServiceImpl{}
}

func (inst* p105ddcbe44_agents_PNPServiceImpl) inject(injext application.InjectionExt, instance any) error {
	ie := injext
	com := instance.(*p105ddcbe4.PNPServiceImpl)
	nop(ie, com)

	
    com.WebClients = inst.getWebClients(ie)
    com.PushServiceURL = inst.getPushServiceURL(ie)
    com.PushToken = inst.getPushToken(ie)
    com.UserName = inst.getUserName(ie)
    com.Password = inst.getPassword(ie)


    return nil
}


func (inst*p105ddcbe44_agents_PNPServiceImpl) getWebClients(ie application.InjectionExt)pdea5a0f47.Clients{
    return ie.GetComponent("#alias-dea5a0f47697e78c03558bf00bc7ff9c-Clients").(pdea5a0f47.Clients)
}


func (inst*p105ddcbe44_agents_PNPServiceImpl) getPushServiceURL(ie application.InjectionExt)string{
    return ie.GetString("${mails.pnp.agent.push2.url}")
}


func (inst*p105ddcbe44_agents_PNPServiceImpl) getPushToken(ie application.InjectionExt)string{
    return ie.GetString("${mails.pnp.agent.push2.token}")
}


func (inst*p105ddcbe44_agents_PNPServiceImpl) getUserName(ie application.InjectionExt)string{
    return ie.GetString("${mails.pnp.agent.push2.username}")
}


func (inst*p105ddcbe44_agents_PNPServiceImpl) getPassword(ie application.InjectionExt)string{
    return ie.GetString("${mails.pnp.agent.push2.password}")
}


