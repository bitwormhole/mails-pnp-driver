package main

import (
	"os"

	"gitee.com/bitwormhole/mails-pnp-driver/modules/pnp"

	"github.com/starter-go/starter"
)

func main() {
	m := pnp.DispatcherModule()
	i := starter.Init(os.Args)
	i.MainModule(m)
	i.WithPanic(true).Run()
}
