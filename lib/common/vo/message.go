package vo

import (
	"encoding/json"

	"gitee.com/bitwormhole/mails-pnp-driver/lib/common/dto"
	"github.com/starter-go/vlog"
)

// Message ... VO
type Message struct {
	Base

	Items []*dto.Message `json:"messages"`
}

// ToJSON ... 转换为 JSON 形式
func (inst *Message) ToJSON() MessageJSON {
	bin, err := json.Marshal(inst)
	if err != nil {
		vlog.Warn("cannot convert to json: %s", err.Error())
		return "{}"
	}
	return MessageJSON(bin)
}

////////////////////////////////////////////////////////////////////////////////

// MessageJSON 以 JSON 的形式表示 vo.Message
type MessageJSON string

// ToMessage  把 JSON 转换为  VO
func (j MessageJSON) ToMessage() (*Message, error) {
	obj := &Message{}
	str := string(j)
	err := json.Unmarshal([]byte(str), obj)
	if err != nil {
		return nil, err
	}
	return obj, nil
}
