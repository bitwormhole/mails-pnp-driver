package drivers

import (
	"gitee.com/bitwormhole/mails-pnp-driver/lib/common/dto"
	"gitee.com/bitwormhole/mails-pnp-driver/lib/common/vo"
	"github.com/starter-go/mails"
)

func convertMessageToVO(src *mails.Message, ty dto.MessageType) (*vo.Message, error) {
	dst := &vo.Message{}
	if src == nil {
		return dst, nil
	}
	addrlist := src.ToAddresses
	for _, to := range addrlist {
		item := &dto.Message{
			FromAddr:    src.FromAddress.String(),
			ToAddr:      to.String(),
			FromUser:    src.FromUser,
			ToUser:      src.ToUser,
			Title:       src.Title,
			ContentType: src.ContentType,
			Content:     string(src.Content),
			Type:        ty,
		}
		dst.Items = append(dst.Items, item)
	}
	return dst, nil
}
