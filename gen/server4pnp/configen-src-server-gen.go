package server4pnp
import (
    pee0586e7d "gitee.com/bitwormhole/mails-pnp-driver/lib/pnp"
    pb8c1571bb "gitee.com/bitwormhole/mails-pnp-driver/src/server/golang/servers"
    pa22d28fba "gitee.com/bitwormhole/mails-pnp-driver/src/server/golang/web/controllers"
    pd1a916a20 "github.com/starter-go/libgin"
     "github.com/starter-go/application"
)

// type pb8c1571bb.PNPServiceImpl in package:gitee.com/bitwormhole/mails-pnp-driver/src/server/golang/servers
//
// id:com-b8c1571bb564d54e-servers-PNPServiceImpl
// class:
// alias:alias-ee0586e7ddfedca367801cc9c58e5879-Service
// scope:singleton
//
type pb8c1571bb5_servers_PNPServiceImpl struct {
}

func (inst* pb8c1571bb5_servers_PNPServiceImpl) register(cr application.ComponentRegistry) error {
	r := cr.NewRegistration()
	r.ID = "com-b8c1571bb564d54e-servers-PNPServiceImpl"
	r.Classes = ""
	r.Aliases = "alias-ee0586e7ddfedca367801cc9c58e5879-Service"
	r.Scope = "singleton"
	r.NewFunc = inst.new
	r.InjectFunc = inst.inject
	return r.Commit()
}

func (inst* pb8c1571bb5_servers_PNPServiceImpl) new() any {
    return &pb8c1571bb.PNPServiceImpl{}
}

func (inst* pb8c1571bb5_servers_PNPServiceImpl) inject(injext application.InjectionExt, instance any) error {
	ie := injext
	com := instance.(*pb8c1571bb.PNPServiceImpl)
	nop(ie, com)

	


    return nil
}



// type pa22d28fba.MessageController in package:gitee.com/bitwormhole/mails-pnp-driver/src/server/golang/web/controllers
//
// id:com-a22d28fbac9b8dc1-controllers-MessageController
// class:class-d1a916a203352fd5d33eabc36896b42e-Controller
// alias:
// scope:singleton
//
type pa22d28fbac_controllers_MessageController struct {
}

func (inst* pa22d28fbac_controllers_MessageController) register(cr application.ComponentRegistry) error {
	r := cr.NewRegistration()
	r.ID = "com-a22d28fbac9b8dc1-controllers-MessageController"
	r.Classes = "class-d1a916a203352fd5d33eabc36896b42e-Controller"
	r.Aliases = ""
	r.Scope = "singleton"
	r.NewFunc = inst.new
	r.InjectFunc = inst.inject
	return r.Commit()
}

func (inst* pa22d28fbac_controllers_MessageController) new() any {
    return &pa22d28fba.MessageController{}
}

func (inst* pa22d28fbac_controllers_MessageController) inject(injext application.InjectionExt, instance any) error {
	ie := injext
	com := instance.(*pa22d28fba.MessageController)
	nop(ie, com)

	
    com.Sender = inst.getSender(ie)
    com.PNPService = inst.getPNPService(ie)
    com.WantPullTokenSum = inst.getWantPullTokenSum(ie)
    com.WantPushTokenSum = inst.getWantPushTokenSum(ie)


    return nil
}


func (inst*pa22d28fbac_controllers_MessageController) getSender(ie application.InjectionExt)pd1a916a20.Responder{
    return ie.GetComponent("#alias-d1a916a203352fd5d33eabc36896b42e-Responder").(pd1a916a20.Responder)
}


func (inst*pa22d28fbac_controllers_MessageController) getPNPService(ie application.InjectionExt)pee0586e7d.Service{
    return ie.GetComponent("#alias-ee0586e7ddfedca367801cc9c58e5879-Service").(pee0586e7d.Service)
}


func (inst*pa22d28fbac_controllers_MessageController) getWantPullTokenSum(ie application.InjectionExt)string{
    return ie.GetString("${mails.pnp.server.pull.want-token-sha256sum}")
}


func (inst*pa22d28fbac_controllers_MessageController) getWantPushTokenSum(ie application.InjectionExt)string{
    return ie.GetString("${mails.pnp.server.push.want-token-sha256sum}")
}


