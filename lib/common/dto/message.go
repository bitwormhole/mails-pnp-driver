package dto

// MessageType 表示消息类型
type MessageType string

// 定义消息类型
const (
	TypeSMS   MessageType = "sms"
	TypeEmail MessageType = "email"
)

// Message ... dto
type Message struct {
	// ID dxo.ExampleID `json:"id"`
	// Base

	Type MessageType `json:"type"`

	ToAddr   string `json:"to"`
	FromAddr string `json:"from"`

	ToUser   string `json:"to_user"`
	FromUser string `json:"from_user"`

	Title       string `json:"title"`
	ContentType string `json:"content_type"`
	Content     string `json:"content"`
}
