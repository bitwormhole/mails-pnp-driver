package pnp

import (
	"context"
	"time"

	"gitee.com/bitwormhole/mails-pnp-driver/lib/common/vo"
)

// Service ... 代表消息的 push & pull 服务
type Service interface {
	Push(c context.Context, msg *vo.Message) error

	Pull(c context.Context, timeout time.Duration) (*vo.Message, error)
}
