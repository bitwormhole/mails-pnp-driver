package vo

import "gitee.com/bitwormhole/mails-pnp-driver/lib/common/dto"

// Example ... VO
type Example struct {
	Base

	Items []*dto.Example `json:"items"`
}
