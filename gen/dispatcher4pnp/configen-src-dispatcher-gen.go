package dispatcher4pnp
import (
    p85f347a2d "gitee.com/bitwormhole/mails-pnp-driver/src/dispatcher/golang/dispatchers"
    pdea5a0f47 "github.com/starter-go/httpagent"
    pd671d76a1 "github.com/starter-go/mails"
     "github.com/starter-go/application"
)

// type p85f347a2d.Engine in package:gitee.com/bitwormhole/mails-pnp-driver/src/dispatcher/golang/dispatchers
//
// id:com-85f347a2d330d063-dispatchers-Engine
// class:
// alias:
// scope:singleton
//
type p85f347a2d3_dispatchers_Engine struct {
}

func (inst* p85f347a2d3_dispatchers_Engine) register(cr application.ComponentRegistry) error {
	r := cr.NewRegistration()
	r.ID = "com-85f347a2d330d063-dispatchers-Engine"
	r.Classes = ""
	r.Aliases = ""
	r.Scope = "singleton"
	r.NewFunc = inst.new
	r.InjectFunc = inst.inject
	return r.Commit()
}

func (inst* p85f347a2d3_dispatchers_Engine) new() any {
    return &p85f347a2d.Engine{}
}

func (inst* p85f347a2d3_dispatchers_Engine) inject(injext application.InjectionExt, instance any) error {
	ie := injext
	com := instance.(*p85f347a2d.Engine)
	nop(ie, com)

	
    com.MailToService = inst.getMailToService(ie)
    com.WebClients = inst.getWebClients(ie)
    com.PullFromURL = inst.getPullFromURL(ie)
    com.PullWithToken = inst.getPullWithToken(ie)


    return nil
}


func (inst*p85f347a2d3_dispatchers_Engine) getMailToService(ie application.InjectionExt)pd671d76a1.Service{
    return ie.GetComponent("#alias-d671d76a169061f84f6814f84b98af24-Service").(pd671d76a1.Service)
}


func (inst*p85f347a2d3_dispatchers_Engine) getWebClients(ie application.InjectionExt)pdea5a0f47.Clients{
    return ie.GetComponent("#alias-dea5a0f47697e78c03558bf00bc7ff9c-Clients").(pdea5a0f47.Clients)
}


func (inst*p85f347a2d3_dispatchers_Engine) getPullFromURL(ie application.InjectionExt)string{
    return ie.GetString("${mails.pnp.dispatcher.pull.url}")
}


func (inst*p85f347a2d3_dispatchers_Engine) getPullWithToken(ie application.InjectionExt)string{
    return ie.GetString("${mails.pnp.dispatcher.pull.token}")
}


